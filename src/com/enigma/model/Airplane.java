package com.enigma.model;

import java.util.Scanner;

public class Airplane extends Vehicle {
    private String flightNum;

//    // getters
//    public String getFlightNum() {
//        return flightNum;
//    }

    // setters
    public void setFlightNum(String newFN) {
        this.flightNum = newFN;
    }

    // methods -- override Vehicle class
//    public void communicate() {
//        System.out.println("Tower, this is flight" + getFlightNum() + ". Requesting permission to land.");
//    }

//    public String toStr() {
//        return "This is an airplane, flight number" + getFlightNum() ;
//
//    }

@Override
    public String toString() {
        return "Airplane{" +
                "Number Airplane='" + flightNum + '\'' +
                "seats=" + seats + '\'' +
                "topspeed=" + topSpeed + '\'' +
                '}';
    }
}
