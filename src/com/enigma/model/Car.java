package com.enigma.model;


public class Car extends Vehicle {
    private int numDoors;
    private String motorType;

    // getters
    public int getNumDoors() {
        return numDoors;
    }

    public String getMotorType() {
        return motorType;
    }

    // setters
    public void setNumDoors(int newND) {
        numDoors = newND;
    }

    public void setMotorType(String newMT) {
        motorType = newMT;
    }

    // methods
    public void communicate() {
        System.out.println("Honk honk!");
    }

    public String toStr() {
        // if plural doors and vowel
        if ((getNumDoors() > 1) && (getMotorType().substring(0, 1).toLowerCase().compareTo("a") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("e") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("i") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("o") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("u") == 0)) {
            return "This is an " + getMotorType() + " car with " + getNumDoors() + " doors.";
        }
        // else if plural doors only
        else if (getNumDoors() > 1) {
            return "This is a " + getMotorType() + " car with " + getNumDoors() + " doors.";
        }
        // else if singular door and vowel in motor
        else if (getMotorType().substring(0, 1).toLowerCase().compareTo("a") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("e") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("i") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("o") == 0 || getMotorType().substring(0, 1).toLowerCase().compareTo("u") == 0) {
            return "This is an " + getMotorType() + " car with " + getNumDoors() + " door.";
        }
        // else singular door no vowel
        else
            return "This is a " + getMotorType() + " car with " + getNumDoors() + " door.";
    }
}


