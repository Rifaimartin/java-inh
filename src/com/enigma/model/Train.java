package com.enigma.model;


import java.util.Scanner;

public class Train extends Vehicle {
    private int boxCars;
    // getters
    public int getBoxCars() {
        return boxCars;
    }
    // setters
    public void setBoxCars(int newBC) {
        boxCars=newBC;
    }
    // methods
    public void communicate() {
        System.out.println("I think I can, I think I can!");
    }
    public String toStr() {
        return "This is a train, all aboard!";
    }
}
