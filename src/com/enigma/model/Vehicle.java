package com.enigma.model;

import java.awt.*;

public class Vehicle {
    protected int seats;
    private int passengers;
    private Point currentPosition;
    private Point destination;
    protected int topSpeed;



    // getters
    public int getSeats() {
        return seats;
    }
    public int getPass() {
        return passengers;
    }
    public Point getCurrentPos() {
        return currentPosition;
    }
    public Point getDestination() {
        return destination;
    }
    public int getTopSpeed() {
        return topSpeed;
    }
    // setters
    public void setSeats(int newSeats) {
        seats = newSeats;
    }
    public void setPass(int newPass) {
        passengers = newPass;
    }
    public void setCurrentPos(Point newCP) {
        currentPosition = newCP;
    }
    public void setDestination(Point newDest) {
        destination = newDest;
    }
    public void setTopSpeed(int newTS) {
        topSpeed = newTS;
    }
    // methods
    public void communicate() {
        System.out.println("Hey you!");
    }
    public String toStr() {
        return "This is a vehicle.";
    }
    public int speedCompareTo(Vehicle v) {
        // returns int representing the difference in speed.
        return v.getTopSpeed()-getTopSpeed();
    }
    public int speedCompareTo(Car c) {
        return c.getTopSpeed() - getTopSpeed();
    }
    public int speedCompareTo(Airplane a) {
        return a.getTopSpeed() - getTopSpeed();
    }
    public int speedCompareTo(Train t) {
        return t.getTopSpeed() - getTopSpeed();
    }

}

